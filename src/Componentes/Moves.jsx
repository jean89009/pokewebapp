import React , {useState , useEffect } from 'react';
import axios from'axios'

const Moves = (props) => {

    useEffect(() => {
        Obtener_move()
    }, [])

    const [ movimiento , setMovimiento] = useState([])
    const [ movimiento_effect , setMovimiento_effect] = useState('')

    const Obtener_move = () => {

        // console.log(props.url)
  
        axios.get(props?.url).then(
            resp=> {
               const datos = resp.data
            //    console.log(datos)
               setMovimiento(datos)
               setMovimiento_effect(datos.effect_entries[1]?.effect)

            }
        )
    }

    const Color = (type) => {
        switch(type) {
            case 'normal' : return '#A8A878';
            case 'electric' : return '#F8D030';
            case 'poison' : return '#A040A0';
            case 'fairy' : return '#F0B6BC';
            case 'ground' : return '#E0C068';
            case 'grass': return '#78C850';
            case 'fire' : return '#F08030';
            case 'water' : return '#6890F0';
            case 'bug' : return '#A8B820';
            case 'ice' : return '#98D8D8';
            case 'fighting': return '#C03028';
            case 'flying' : return '#A890F0';
            case 'psychic' : return '#F85888';
            case 'rock' : return '#B8A038';
            case 'ghost' : return '#705898';
            case 'dark' : return '#705848';
            case 'dragon' : return '#7038F8';
            case 'steel' : return '#B8B8D0';
            default : return 'black'
        }
      }

    return ( 
        <div style={ { backgroundColor: Color(movimiento?.type?.name) } } className="col-12 text-white hab-card mb-3 ">
            <div className="d-flex align-items-center mb-1">
            <table >
                <tr>
                    <td>Name:</td> <td>PP:</td> <td>Accuracy:</td> <td>Power:</td>
                </tr>
                <tr>
                    <td>{movimiento.name}</td> <td>{movimiento.pp}</td> <td>{movimiento.accuracy === null ? '----' : movimiento.accuracy } {movimiento.accuracy === null ? '' : '%' } </td> <td>  {movimiento.power === null ? '---' : movimiento.power }</td>
                </tr>
            </table>
            </div>
            <div className=" ">
            <table >
                <tr>
                    <td className='text-init pl-2' >Learned at level: {props.learn_level} </td>
                    <td>Priority: </td>
                </tr>
                <tr>
                    <td className='text-init pl-2' >Learn method: {props.learn_method}</td> <td className='' > {movimiento.priority}</td> 
                </tr>
            </table>
            </div>
        </div>
     );
}
 
export default Moves;
