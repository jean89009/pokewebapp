import React, { useEffect, useState } from 'react';
import { Link} from "react-router-dom";
import img_default from '../img/ms.png'

import axios from 'axios'

const Cards = (props) => {

    useEffect(() => {
        Obtener_pokemon()
    }, [])

    const [pokemon , SetPokemon] = useState([{}])
    const [tipo , setTipo] = useState([])

    const Obtener_pokemon = () => {

        axios.get(props.url).then(
            resp=> {
               const datos = resp.data
            //    console.log(datos)
               setTipo(datos.types[0].type.name)
               SetPokemon(datos)
            }
        )
    }

    const Color = (type) => {
        switch(type) {
            case 'normal' : return '#A8A878';
            case 'electric' : return '#F8D030';
            case 'poison' : return '#A040A0';
            case 'fairy' : return '#F0B6BC';
            case 'ground' : return '#E0C068';
            case 'grass': return '#78C850';
            case 'fire' : return '#F08030';
            case 'water' : return '#6890F0';
            case 'bug' : return '#A8B820';
            case 'ice' : return '#98D8D8';
            case 'fighting': return '#C03028';
            case 'fly' : return '#A890F0';
            case 'psychic' : return '#F85888';
            case 'rock' : return '#B8A038';
            case 'ghost' : return '#705898';
            case 'dark' : return '#705848';
            case 'dragon' : return '#7038F8';
            case 'steel' : return '#B8B8D0';
            default : return 'white'
        }
      }

    return ( 
        
        <Link className='row col-6 col-md-2 col-xxl-3' to={ pokemon.name ? `/Pokemon/${pokemon.name}` : '/' } > 

            <div className="col-12  row ">

                <div className=" relative p-4 w-100">

                    <div style={ { backgroundColor: Color(tipo) }  } className="badge">
                        <p  className="pokext"> #{pokemon.id} - {pokemon.name} </p>
                    </div>

                   <img className='poke_img' src={ !pokemon?.sprites?.front_default ? img_default :  pokemon?.sprites?.front_default} alt="pokemon" ></img>

                </div>

            </div>

        </Link>
        
     );
}
 
export default Cards;