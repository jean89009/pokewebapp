import React from 'react';
import Logo from '../img/pokeballs.png'
import { NavLink} from "react-router-dom";

const Navbar = () => {
    return ( 
       <div className='w-100 row ' >

        <div className="col-6 pt-2 pl-4 row">

            <div className="col-6 col-md-2">
                <img src={Logo} className="w-90" ></img>
            </div>

            {/* <div className="col-10 d-flex align-items-center">
                <input className='search' placeholder='Search pokemon' ></input>
            </div> */}

        </div>

        <div className="col-6 align-items-center justify-content-end pt-2 pl-4 row">
          
            <NavLink to="/"  > <button className="nav-btn" >Pokemons</button> </NavLink>

        </div>
          
       </div>
     );
}
 
export default Navbar;