import React from 'react';
import missing from '../img/ms.png'

const Loader = (props) => {
    return ( 
    <div  className={ props.missing ? 'loader bg-white' : "loader bg-fade" } >
       <div className="loader-content row">
         <img src={ props.missing ? missing : "https://media1.giphy.com/media/Xdv29zqFlaSlO/giphy.gif?cid=ecf05e475n6de1dubucoiswwx6fyp2hhgs63s36fhkcgqzry&rid=giphy.gif&ct=s" } className='col-12 col-md-3' alt="" />
         <div className="col-12 d-flex justify-content-center">
            <p className='loader-text animate__animated animate__pulse animate__delay-2s 2s animate__infinite infinite ' >{props.mensaje}</p>
         </div>
       </div>
    </div> );
}
 
export default Loader;