import React , { useState, useEffect } from 'react';
import axios from 'axios'

const Habilidades = (props) => {

    useEffect(() => {
        Obtener_habilidad()
    }, [])

    const [ habilidad , setHabilidad] = useState([])
    const [ habilidad_effect , setHabilidad_effect] = useState('')

    const Obtener_habilidad = () => {
  
        axios.get(props?.url).then(
            resp=> {
               const datos = resp.data
            //    console.log(datos)
               setHabilidad(datos)
               setHabilidad_effect(datos.effect_entries[1]?.effect)

            }
        )
    }

    const Color = (type) => {
        switch(type) {
            case 'normal' : return '#A8A878';
            case 'electric' : return '#F8D030';
            case 'poison' : return '#A040A0';
            case 'fairy' : return '#F0B6BC';
            case 'ground' : return '#E0C068';
            case 'grass': return '#78C850';
            case 'fire' : return '#F08030';
            case 'water' : return '#6890F0';
            case 'bug' : return '#A8B820';
            case 'ice' : return '#98D8D8';
            case 'fighting': return '#C03028';
            case 'fly' : return '#A890F0';
            case 'psychic' : return '#F85888';
            case 'rock' : return '#B8A038';
            case 'ghost' : return '#705898';
            case 'dark' : return '#705848';
            case 'dragon' : return '#7038F8';
            case 'steel' : return '#B8B8D0';
            default : return 'white'
        }
      }

    return ( 
        <div className="col-12 hab-card mb-3 ">
            <div className="d-flex align-items-center mb-3">
              <p className='type_index mb-0' style={ { backgroundColor: Color(props.tipo) } } > {props.index + 1} </p>
              <p className='title-hab bold' >  {habilidad.name} { props.hidden ? <span className='hidden' > Hidden </span> : <span></span>  } </p>
            </div>
              <p> {habilidad_effect} </p>
        </div>
     );
}
 
export default Habilidades;