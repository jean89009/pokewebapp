import React, { useState , useEffect } from 'react';
import Cards from '../Componentes/Cards';
import Logo from '../img/pokeballs.png'
import { NavLink} from "react-router-dom";
import axios from 'axios'
import Loader from '../Componentes/Loader';

const Inicio = () => {

    useEffect(() => {
        fetchData()
    }, [])

    const [pokemons, setPokemons] = useState([{}])
    const [pokemon , setPokemon] = useState([{}])
    const [isloading , setIsloading] = useState(false)
    const [search , setSearch] = useState('')

    const fetchData = async () => {

        setIsloading(true)

        try {

            axios.get('https://pokeapi.co/api/v2/pokemon?limit=151').then(
                resp=> {
                   const datos = resp.data.results
                //    console.log(datos)
                   setPokemons(datos)
                   setPokemon(datos)
                   setIsloading(false)
    
                }
            )
            
        } catch (error) {

            console.log(error)
            
        }

    }

    // Filtro

    const searcher = (e) => {
        setSearch(e.target.value)
    }

   const results = !search ? pokemons : pokemons.filter((dato)=> dato.name.toLowerCase().includes(search.toLocaleLowerCase()))  




    return ( 
        <div className='d-flex bg-night align-items-center vh-100 justify-content-center' >

            {isloading ? <Loader mensaje="Looking for pokemon please wait" missing={false} ></Loader> : <div className=""></div> }

            <div className='dash px-4' >

                <div className='w-100 row ' >

                    <div className="col-12 col-md-6 pt-2 pl-logo row">

                        <div className="col-3 col-md-2">
                            <img src={Logo} className="w-90" alt='logo' ></img>
                        </div>

                        <div className="col-9 col-md-10 d-flex align-items-center">
                            <input value={search} onChange={searcher} className='search' placeholder='Search pokemon' ></input>
                        </div>

                    </div>

                    <div className="col-12 col-md-6 align-items-center justify-content-end pt-2 pl-4 row hide-sm ">
                    
                        <NavLink to="/"  > <button className="nav-btn" >Pokemons</button> </NavLink>

                    </div>
                
                </div>

                <div className='row w-100  ' >

                    {isloading ? <div className=""></div> 

                     :

                     results.map(  (item,index) => <Cards className="" key={index}  name={item.name} url={item.url} ></Cards>) 
                      
                    }

                </div>

            </div>

        </div>
     );
}
 
export default Inicio;