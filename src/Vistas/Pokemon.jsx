import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {useParams} from 'react-router-dom'
import { FaSearch} from 'react-icons/fa';
// Componentes
import Habilidades from '../Componentes/Habilidades';
import Moves from '../Componentes/Moves';
import Navbar from '../Componentes/Navbar';
import Loader from '../Componentes/Loader';

const Pokemon = () => {

  useEffect(() => {
        Obtener_pokemon()
    }, [])

  const [pokemon_data , SetPokemon_data] = useState([{}])
  const [pokemon_img , SetPokemon_img] = useState('')
  const [stats , SetStats] = useState([{}])
  const [habilidad , SetHabilidad] = useState([{}])
  const [moves , SetMoves] = useState([{}])
  const [moves_details , SetMoves_details] = useState([{}])
  const [tipos , SetTipos] = useState([])
  const [types , SetTypes] = useState('')
  const [isloading , setIsloading] = useState(true)
  const [not_found , setNot_found] = useState(false)
  const [ search, setSearch ] = useState("")
  const {pokemon} = useParams();

  const Obtener_pokemon = async () => {

    setIsloading(true)

    try {

      await axios.get('https://pokeapi.co/api/v2/pokemon/'+pokemon).then(
        resp=> {
            const datos = resp.data
            // console.log(datos)
            SetStats(datos.stats)
            SetHabilidad(datos.abilities)
            SetMoves(datos.moves)
            SetMoves_details(datos.moves[0].version_group_details[0])
            SetPokemon_img(datos.sprites.front_default)
            SetPokemon_data(datos)
            SetTipos(datos.types)
            SetTypes(datos?.types[0]?.type?.name)
        }
    )
      
    } catch (err) {

      setNot_found(true)
      
    }

    finally{
      setIsloading(false)
    }
     
  }

  const searcher = (e) => {
      setSearch(e.target.value)   
  }

  const results = !search ? moves : moves.filter((dato)=> dato?.move?.name.toLowerCase().includes(search.toLocaleLowerCase()))

  const statColor = (name) => {
      switch(name) {
          case 'hp' : return '#78C850';
          case 'attack' : return '#F08030';
          case 'defense' : return '#B8B8D0';
          case 'special-attack' : return '#F85888';
          case 'special-defense' : return '#6890F0';
          case 'speed' : return 'rgb(221 186 48)';
          default : return 'white'
      }
  }

  const watch_img = (img) => {
    SetPokemon_img(img)
  }

  const Color = (type) => {
    switch(type) {
        case 'normal' : return '#A8A878';
        case 'electric' : return '#F8D030';
        case 'poison' : return '#A040A0';
        case 'fairy' : return '#F0B6BC';
        case 'ground' : return '#E0C068';
        case 'grass': return '#78C850';
        case 'fire' : return '#F08030';
        case 'water' : return '#6890F0';
        case 'bug' : return '#A8B820';
        case 'ice' : return '#98D8D8';
        case 'fighting': return '#C03028';
        case 'flying' : return '#A890F0';
        case 'psychic' : return '#F85888';
        case 'rock' : return '#B8A038';
        case 'ghost' : return '#705898';
        case 'dark' : return '#705848';
        case 'dragon' : return '#7038F8';
        case 'steel' : return '#B8B8D0';
        default : return 'white'
    }
  }

  return ( 
    <div className="">

      {not_found ? <Loader mensaje="Oops you found a missingno, please go back" missing={true} ></Loader> : <div className=""></div> }
      
      { isloading ? <Loader mensaje="receiving data please wait" missing={false} ></Loader> : <div className=""></div> }

      <div className="contenedor">

        <div className="contenido pb-5 row">

          <Navbar  ></Navbar>


          <div className="col-12 col-md-4 p-3 mt-2 justify-content-center relative row">

            <div style={ { backgroundColor: Color(types) }  } className="badge_poke_card">
                <p className='mb-0' > #{pokemon_data.id}-{pokemon_data?.name} </p>
            </div>

            <div className="col-12 poke_portrait shadow py-3 jusrify-content-center row ">

              <div className="col-7 relative">

                <div  className="absolute_types">
                {tipos.map( (item,index) =>
                
                  <div style={ { backgroundColor: Color(item?.type?.name) }  } key={index} className="badge_types">
                    <p className='mb-0' > {item?.type?.name} </p>
                  </div>
                
                )}
                </div>

                <img src={pokemon_img} className="poke_img" alt="" />

              </div>

              <div className="display_img row col-12 ">

                <div className="col-3 px-2 ">

                  <img src={pokemon_data?.sprites?.front_default} onClick={ () => watch_img(pokemon_data?.sprites?.front_default) } className="w-100 mini-img shadow-sm" alt="" />

                </div>

                <div className="col-3 px-2 ">

                  <img src={pokemon_data?.sprites?.back_default} onClick={ () => watch_img(pokemon_data?.sprites?.back_default) } className="w-100 mini-img shadow-sm" alt="" />

                </div>

                <div className="col-3 px-2 ">

                  <img src={pokemon_data?.sprites?.front_shiny} onClick={ () => watch_img(pokemon_data?.sprites?.front_shiny) } className="w-100 mini-img shadow-sm" alt="" />

                </div>

                <div className="col-3 px-2 ">

                  <img src={pokemon_data?.sprites?.back_shiny} onClick={ () => watch_img(pokemon_data?.sprites?.back_shiny) } className="w-100 mini-img shadow-sm" alt="" />

                </div>

              </div>

              <div className="col-12 mt-3 px-3 row">
                
                { stats.map( (item, index) =>
                
                <div key={index} style={ { width: item?.base_stat+'%' , backgroundColor: statColor(item?.stat?.name) }  }  className=" my-1 stat ">
                  <p className='mb-0 ml-3' > {item?.stat?.name} = {item?.base_stat} </p>
                </div> 
                
                ) }

              </div>

            </div>

          </div>

          <div className="col-12 col-md-4 p-3  mt-2 justify-content-center relative row">

            <div style={ { backgroundColor: Color(types) }  } className="badge_poke_card">
                <p className='mb-0' >  Abilities </p>
            </div>

            <div className="col-12 poke_portrait shadow py-3 px-2 justify-content-center row">
              <div className="col-12 p-0 height-fit">
                { !isloading ? habilidad.map( (item,index) =><Habilidades hidden={item?.is_hidden} tipo={types} key={index} index={index} url={item?.ability?.url} ></Habilidades>) : <div className=""></div> }
              </div>
            </div>

          </div>

          <div className="col-12 col-md-4 p-3  mt-2 justify-content-center relative row">

            <div style={ { backgroundColor: Color(types) }  } className="badge_poke_card">
                <p className='mb-0' > Moves </p>
            </div>

            <div className="col-12 poke_portrait shadow py-3 px-2 justify-content-center row">

             

              <div className="col-12 p-0 height-fit">

              <div className="d-flex my-3 w-100">

                <input placeholder='Search Move ...' value={search} onChange={searcher} className='search' type="text" />

                <button className='btn_search' style={ { backgroundColor: Color(types) }  } > <FaSearch /> </button>

              </div>

                { !isloading ? results.map( (item,index) =><Moves key={index} learn_level={moves_details?.level_learned_at} learn_method={moves_details?.move_learn_method?.name} url={item?.move?.url} ></Moves>) : <div className=""></div> }
              </div>
            </div>

          </div>

        </div>

      </div>

    </div>
       
     );
}
 
export default Pokemon;