import './App.css';
import { BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Inicio from './Vistas/Inicio';
import Pokemon from './Vistas/Pokemon';

function App() {
  return (
    <div>

      <Router>
        <Routes>
          <Route path='/' element={<Inicio></Inicio>} ></Route>
          <Route path='/Pokemon/:pokemon' element={<Pokemon></Pokemon>} ></Route>
        </Routes>
      </Router>
     
    </div>
  );
}

export default App;
